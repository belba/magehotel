package com.magehotel.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magehotel.backend.apirest.models.dao.ICategoriaDao;
import com.magehotel.backend.apirest.models.entity.Categoria;

@Service
public class CategoriaServiceImpl implements ICategoriaService{

	@Autowired
	ICategoriaDao iCategoriaDAO;
	
	@Override
	public List<Categoria> listCategoria() {
		// TODO Auto-generated method stub
		return iCategoriaDAO.findAll();
	}

	@Override
	public Categoria saveCategoria(Categoria categoria) {
		// TODO Auto-generated method stub
		return iCategoriaDAO.save(categoria);
	}

	@Override
	public Categoria categoriaXID(Integer id) {
		// TODO Auto-generated method stub
		return iCategoriaDAO.findById(id).get();
	}

	@Override
	public Categoria updateCategoria(Categoria categoria) {
		// TODO Auto-generated method stub
		return iCategoriaDAO.save(categoria);
	}

	@Override
	public void deleteCategoria(Integer id) {
		// TODO Auto-generated method stub
		iCategoriaDAO.deleteById(id);
	}

}
