package com.magehotel.backend.apirest.auth.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magehotel.backend.apirest.auth.enums.RolNombre;
import com.magehotel.backend.apirest.models.entity.Rol;

@Repository
public interface RoleRepository extends JpaRepository<Rol, Integer>{
	
	Optional<Rol> findByRolNombre(RolNombre rolNombre);

}
