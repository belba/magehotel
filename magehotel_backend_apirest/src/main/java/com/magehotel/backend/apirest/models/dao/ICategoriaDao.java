package com.magehotel.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magehotel.backend.apirest.models.entity.Categoria;

public interface ICategoriaDao extends JpaRepository<Categoria, Integer>{

}
