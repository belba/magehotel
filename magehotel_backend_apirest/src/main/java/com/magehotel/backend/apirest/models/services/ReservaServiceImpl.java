package com.magehotel.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magehotel.backend.apirest.models.dao.IReservaDao;
import com.magehotel.backend.apirest.models.entity.Reserva;

@Service
public class ReservaServiceImpl implements IReservaService{
	@Autowired
	IReservaDao iReservaDao;

	@Override
	public List<Reserva> listReserva() {
		// TODO Auto-generated method stub
		return iReservaDao.findAll();
	}

	@Override
	public Reserva saveReserva(Reserva reserva) {
		// TODO Auto-generated method stub
		return iReservaDao.save(reserva);
	}

	@Override
	public Reserva reservaXID(Integer id) {
		// TODO Auto-generated method stub
		return iReservaDao.findById(id).get();
	}

	@Override
	public Reserva updateReserva(Reserva reserva) {
		// TODO Auto-generated method stub
		return iReservaDao.save(reserva);
	}

	@Override
	public void deleteReserva(Integer id) {
		// TODO Auto-generated method stub
		iReservaDao.deleteById(id);
	}
}
