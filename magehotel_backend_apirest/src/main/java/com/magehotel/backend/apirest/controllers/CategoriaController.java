package com.magehotel.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magehotel.backend.apirest.models.entity.Categoria;
import com.magehotel.backend.apirest.models.services.CategoriaServiceImpl;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/api")
public class CategoriaController {
	@Autowired
	CategoriaServiceImpl categoriaServiceImpl; 
	
	@GetMapping("/categorias")
	public List<Categoria> listarCategoria(){
		return categoriaServiceImpl.listCategoria();
	}
	
	
	@PostMapping("/categorias")
	public Categoria salvarCategoria(@RequestBody Categoria categoria) {
		
		return categoriaServiceImpl.saveCategoria(categoria);
	}
	
	
	@GetMapping("/categorias/{id}")
	public Categoria categoriaXID(@PathVariable(name="id") Integer id) {
		
		Categoria categoria_xid= new Categoria();
		
		categoria_xid=categoriaServiceImpl.categoriaXID(id);
		
		System.out.println("Categoria XID: "+categoria_xid);
		
		return categoria_xid;
	}
	
	@PutMapping("/categorias/{id}")
	public Categoria actualizarCategoria(@PathVariable(name="id")Integer id,@RequestBody Categoria categoria) {
		
		Categoria categoria_seleccionado= new Categoria();
		Categoria categoria_actualizado= new Categoria();
		
		categoria_seleccionado= categoriaServiceImpl.categoriaXID(id);
		
		categoria_seleccionado.setNombre(categoria.getNombre());
		
		categoria_seleccionado.setAforo(categoria.getAforo());
		
		categoria_seleccionado.setPrecio(categoria.getPrecio());
		
		categoria_actualizado = categoriaServiceImpl.updateCategoria(categoria_seleccionado);
		
		System.out.println("La categoria actualizada es: "+ categoria_actualizado);
		
		return categoria_actualizado;
	}
	
	@DeleteMapping("/categorias/{id}")
	public void eliminarCategoria(@PathVariable(name="id")Integer id) {
		categoriaServiceImpl.deleteCategoria(id);
	}
}
