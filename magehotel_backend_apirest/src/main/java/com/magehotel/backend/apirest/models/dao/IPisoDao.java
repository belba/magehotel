package com.magehotel.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magehotel.backend.apirest.models.entity.Piso;

public interface IPisoDao extends JpaRepository<Piso, Integer> {

}
