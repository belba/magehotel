package com.magehotel.backend.apirest.models.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;

@Entity
@Table(name="reservas", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "fecha_inicio", "habitacion" }, name = "UQ_Reserva_fecha_inicio_habitacion") })
public class Reserva {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@NotNull
    @JoinColumn(name = "habitacion", foreignKey=@ForeignKey(name="FK_Reserva_Habitacion"), nullable = false)
    Habitacion habitacion;
	
	@ManyToOne
	@NotNull
    @JoinColumn(name = "cliente", foreignKey=@ForeignKey(name="FK_Reserva_Cliente"), nullable = false)
    Cliente cliente;
	
	@Column(name = "fecha_inicio", nullable = false)
	@NotNull
	@JsonFormat(pattern="yyyy-MM-dd")
	LocalDate fecha_inicio;
	
	@Column(name = "fecha_final", nullable = false)
	@NotNull
	@JsonFormat(pattern="yyyy-MM-dd")
	LocalDate fecha_final;
	
	@Column(name = "estado", nullable = false)
	@NotNull
	int estado;

	public Reserva() {
		
	}
	
	public Reserva(int id, Habitacion habitacion, Cliente cliente, LocalDate fecha_inicio, LocalDate fecha_final,
			int estado) {
		this.id = id;
		this.habitacion = habitacion;
		this.cliente = cliente;
		this.fecha_inicio = fecha_inicio;
		this.fecha_final = fecha_final;
		this.estado = estado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Habitacion getHabitacion() {
		return habitacion;
	}

	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public LocalDate getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(LocalDate fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public LocalDate getFecha_final() {
		return fecha_final;
	}

	public void setFecha_final(LocalDate fecha_final) {
		this.fecha_final = fecha_final;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
	
	
}
