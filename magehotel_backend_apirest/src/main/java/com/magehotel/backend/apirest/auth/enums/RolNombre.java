package com.magehotel.backend.apirest.auth.enums;

public enum RolNombre {
	ROLE_ADMIN, ROLE_USER
}
