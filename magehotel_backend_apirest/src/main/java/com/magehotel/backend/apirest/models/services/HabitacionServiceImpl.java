package com.magehotel.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magehotel.backend.apirest.models.dao.IHabitacionDao;
import com.magehotel.backend.apirest.models.entity.Habitacion;

@Service
public class HabitacionServiceImpl implements IHabitacionService{
	@Autowired
	IHabitacionDao iHabitacionDao;
	
	@Override
	public List<Habitacion> listHabitacion() {
		// TODO Auto-generated method stub
		return iHabitacionDao.findAll();
	}

	@Override
	public Habitacion saveHabitacion(Habitacion habitacion) {
		// TODO Auto-generated method stub
		return iHabitacionDao.save(habitacion);
	}

	@Override
	public Habitacion habitacionXID(Integer id) {
		// TODO Auto-generated method stub
		return iHabitacionDao.findById(id).get();
	}

	@Override
	public Habitacion updateHabitacion(Habitacion habitacion) {
		// TODO Auto-generated method stub
		return iHabitacionDao.save(habitacion);
	}

	@Override
	public void deleteHabitacion(Integer id) {
		// TODO Auto-generated method stub
		iHabitacionDao.deleteById(id);
	}

}
