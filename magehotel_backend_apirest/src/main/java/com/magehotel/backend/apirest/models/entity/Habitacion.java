package com.magehotel.backend.apirest.models.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

@Entity
@Table(name = "habitaciones", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "piso", "puerta" }, name = "UQ_Habitacion_puerta_piso") })
public class Habitacion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@NotNull
	@JoinColumn(name = "categoria", foreignKey = @ForeignKey(name = "FK_Habitacion_Categoria"), nullable = false)
	Categoria categoria;

	@ManyToOne
	@NotNull
	@JoinColumn(name = "piso", foreignKey = @ForeignKey(name = "FK_Habitacion_Piso"), nullable = false)
	Piso piso;

	@Column(name = "puerta", nullable = false)
	@NotNull
	private int puerta;

	@Column(name = "detalles", length = 100)
	private String detalles;

	@Column(name = "estado", nullable = false)
	private int estado;

	@OneToMany
	@JoinColumn(name = "habitacion")
	private List<Reserva> reserva;

	public Habitacion() {

	}

	public Habitacion(int id, Categoria categoria, Piso piso, int puerta, String detalles, List<Reserva> reserva,
			int estado) {
		this.id = id;
		this.categoria = categoria;
		this.piso = piso;
		this.puerta = puerta;
		this.detalles = detalles;
		this.reserva = reserva;
		this.estado = estado;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Piso getPiso() {
		return piso;
	}

	public void setPiso(Piso piso) {
		this.piso = piso;
	}

	public int getPuerta() {
		return puerta;
	}

	public void setPuerta(int puerta) {
		this.puerta = puerta;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Reserva")
	public List<Reserva> getReserva() {
		return reserva;
	}

	public void setReserva(List<Reserva> reserva) {
		this.reserva = reserva;
	}

	public String getDetalles() {
		return detalles;
	}

	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}

}
