package com.magehotel.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magehotel.backend.apirest.models.entity.Piso;
import com.magehotel.backend.apirest.models.services.PisoServiceImpl;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class PisoController {
	
	@Autowired
	PisoServiceImpl pisoServiceImpl; 
	
	@GetMapping("/pisos")
	public List<Piso> listarPiso(){
		return pisoServiceImpl.listPiso();
	}
	
	
	@PostMapping("/pisos")
	public Piso salvarPiso(@RequestBody Piso piso) {
		
		return pisoServiceImpl.savePiso(piso);
	}
	
	@GetMapping("/pisos/{id}")
	public Piso pisoXID(@PathVariable(name="id") Integer id) {
		
		Piso piso_xid= new Piso();
		
		piso_xid=pisoServiceImpl.pisoXID(id);
		
		return piso_xid;
	}
	
	@PutMapping("/pisos/{id}")
	public Piso actualizarPiso(@PathVariable(name="id")Integer id,@RequestBody Piso piso) {
		
		Piso piso_seleccionado= new Piso();
		Piso piso_actualizado= new Piso();
		
		piso_seleccionado= pisoServiceImpl.pisoXID(id);
		
		piso_seleccionado.setNombre(piso.getNombre());
		
		
		piso_actualizado = pisoServiceImpl.updatePiso(piso_seleccionado);
		
		return piso_actualizado;
	}
	
	@DeleteMapping("/pisos/{id}")
	public void eliminarPiso(@PathVariable(name="id")Integer id) {
		pisoServiceImpl.deletePiso(id);
	}
}
