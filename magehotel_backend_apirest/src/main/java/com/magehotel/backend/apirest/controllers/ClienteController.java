package com.magehotel.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magehotel.backend.apirest.models.entity.Cliente;
import com.magehotel.backend.apirest.models.services.IClienteService;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/api")
public class ClienteController {
//Cliente Controler ////
	@Autowired
	IClienteService clienteService;

	@GetMapping("/clientes")
	public List<Cliente> getAllCliente() {
		return clienteService.getAllClientes();
	}

	@GetMapping("/clientes/{id}")
	public Cliente getCliente(@PathVariable(name = "id") Long id) {
		Cliente cli = new Cliente();
		cli = clienteService.getCliente(id);
		return cli;
	}

	@PostMapping("/clientes")
	public Cliente saveCliente(@RequestBody Cliente cliente) {
		return clienteService.saveCliente(cliente);
	}

	@PutMapping("/clientes/{id}")
	public Cliente updateCliente(@PathVariable(name = "id") Long id, @RequestBody Cliente cliente) {

		Cliente cli_selected = new Cliente();
		Cliente cli_updated = new Cliente();

		cli_selected = clienteService.getCliente(id);

		cli_selected.setDni(cliente.getDni());
		cli_selected.setNombre(cliente.getNombre());
		cli_selected.setTelefono(cliente.getTelefono());
		cli_selected.setEmail(cliente.getEmail());

		cli_updated = clienteService.updateCliente(cli_selected);

		return cli_updated;

	}

	@DeleteMapping("/clientes/{id}")
	public void deleteCliente(@PathVariable(name = "id") Long id) {
		clienteService.deleteCliente(id);
	}
}
