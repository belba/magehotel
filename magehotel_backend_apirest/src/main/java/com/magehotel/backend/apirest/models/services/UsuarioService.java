package com.magehotel.backend.apirest.models.services;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magehotel.backend.apirest.auth.repository.UsuarioRepository;
import com.magehotel.backend.apirest.models.entity.Categoria;
import com.magehotel.backend.apirest.models.entity.Usuario;

@Service
@Transactional
public class UsuarioService {
		
    @Autowired
    UsuarioRepository usuarioRepository;
    
    public List<Usuario> listUsuario() {
		// TODO Auto-generated method stub
		return usuarioRepository.findAll();
	}

    public Optional<Usuario> getByNombreUsuario(String nombreUsuario){
        return usuarioRepository.findByNombreUsuario(nombreUsuario);
    }
    
    public Usuario usuarioXID(Integer id) {
		// TODO Auto-generated method stub
		return usuarioRepository.findById(id).get();
	}

    public boolean existsByNombreUsuario(String nombreUsuario){
        return usuarioRepository.existsByNombreUsuario(nombreUsuario);
    }

    public boolean existsByEmail(String email){
        return usuarioRepository.existsByEmail(email);
    }

    public Usuario save(Usuario usuario){
        return usuarioRepository.save(usuario);
    }
    
    public void deleteUsuario(Integer id) {
    	usuarioRepository.deleteById(id);
    }
}
