package com.magehotel.backend.apirest.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.security.core.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magehotel.backend.apirest.auth.dto.JwtDto;
import com.magehotel.backend.apirest.auth.dto.LoginUsuario;
import com.magehotel.backend.apirest.auth.dto.Mensaje;
import com.magehotel.backend.apirest.auth.dto.NuevoUsuario;
import com.magehotel.backend.apirest.auth.enums.RolNombre;
import com.magehotel.backend.apirest.auth.jwt.JwtProvider;
import com.magehotel.backend.apirest.models.entity.Rol;
import com.magehotel.backend.apirest.models.entity.Usuario;
import com.magehotel.backend.apirest.models.services.RoleService;
import com.magehotel.backend.apirest.models.services.UsuarioService;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = { "http://localhost:4200" }, maxAge = 3600)
public class AuthController {


    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    RoleService rolService;

    @Autowired
    JwtProvider jwtProvider;
    
    @GetMapping("/usuarios")
	public List<Usuario> listarUsuario(){
		return usuarioService.listUsuario();
	}
    
    @GetMapping("/usuarios/{id}")
	public Usuario usuarioXID(@PathVariable(name="id") Integer id) {
		
		Usuario usuario_xid= new Usuario();
		
		usuario_xid=usuarioService.usuarioXID(id);
		
		System.out.println("Usuario XID: "+usuario_xid);
		
		return usuario_xid;
	}

    @PutMapping("/usuarios/{id}")
	public Usuario actualizarUsuario(@PathVariable(name="id")Integer id,@RequestBody NuevoUsuario usuario) {
		
		Usuario usuario_seleccionado= new Usuario();
		Usuario usuario_actualizado= new Usuario();
		
		usuario_seleccionado= usuarioService.usuarioXID(id);
		
		usuario_seleccionado.setNombre(usuario.getNombre());
		
		usuario_seleccionado.setEmail(usuario.getEmail());
		usuario_seleccionado.setNombreUsuario(usuario.getNombreUsuario());
		usuario_seleccionado.setPassword(passwordEncoder.encode(usuario.getPassword()));
		Set<Rol> roles = new HashSet<>();
		roles.add(rolService.getByRolNombre(RolNombre.ROLE_USER).get());
        if(usuario.getRoles().contains("admin"))
        	roles.add(rolService.getByRolNombre(RolNombre.ROLE_ADMIN).get());
		usuario_seleccionado.setRoles(roles);
		
		usuario_actualizado = usuarioService.save(usuario_seleccionado);
		
		System.out.println("El usuari actualizat es: "+ usuario_actualizado);
		
		return usuario_actualizado;
	}
    
   /* @PostMapping("/usuarios/{id}")
   	public Usuario crearUsuario(@RequestBody Usuario usuario) {
   		
   		Set<Rol> roles = new HashSet<>();
   		roles.add(rolService.getByRolNombre(RolNombre.ROLE_USER).get());
           if(usuario.getRoles().contains("admin"))
           	roles.add(rolService.getByRolNombre(RolNombre.ROLE_ADMIN).get());
   		usuario_seleccionado.setRoles(roles);
   		
   		usuario_actualizado = usuarioService.save(usuario_seleccionado);
   		
   		System.out.println("La reserva actualizada es: "+ usuario_actualizado);
   		
   		return usuario_actualizado;
   	}*/
    
    @PostMapping("/usuarios")
    public ResponseEntity<?> createUsuarios(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult){
    	System.out.println(nuevoUsuario.getRoles());
    	System.out.println(nuevoUsuario.getNombre());
        if(bindingResult.hasErrors())
            return new ResponseEntity(new Mensaje("campos mal puestos o email inválido"), HttpStatus.BAD_REQUEST);
        if(usuarioService.existsByNombreUsuario(nuevoUsuario.getNombreUsuario()))
            return new ResponseEntity(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
        if(usuarioService.existsByEmail(nuevoUsuario.getEmail()))
            return new ResponseEntity(new Mensaje("ese email ya existe"), HttpStatus.BAD_REQUEST);
        Usuario usuario =
                new Usuario(nuevoUsuario.getNombre(), nuevoUsuario.getNombreUsuario(), nuevoUsuario.getEmail(),
                        passwordEncoder.encode(nuevoUsuario.getPassword()));
        Set<Rol> roles = new HashSet<>();
        roles.add(rolService.getByRolNombre(RolNombre.ROLE_USER).get());
        if(nuevoUsuario.getRoles().contains("admin"))
            roles.add(rolService.getByRolNombre(RolNombre.ROLE_ADMIN).get());
        usuario.setRoles(roles);
        usuarioService.save(usuario);
        return new ResponseEntity(new Mensaje("usuario guardado"), HttpStatus.CREATED);
    }
  
    @PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult){
    	System.out.println(nuevoUsuario.getRoles());
    	System.out.println(nuevoUsuario.getNombre());
        if(bindingResult.hasErrors())
            return new ResponseEntity(new Mensaje("campos mal puestos o email inválido"), HttpStatus.BAD_REQUEST);
        if(usuarioService.existsByNombreUsuario(nuevoUsuario.getNombreUsuario()))
            return new ResponseEntity(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
        if(usuarioService.existsByEmail(nuevoUsuario.getEmail()))
            return new ResponseEntity(new Mensaje("ese email ya existe"), HttpStatus.BAD_REQUEST);
        Usuario usuario =
                new Usuario(nuevoUsuario.getNombre(), nuevoUsuario.getNombreUsuario(), nuevoUsuario.getEmail(),
                        passwordEncoder.encode(nuevoUsuario.getPassword()));
        Set<Rol> roles = new HashSet<>();
        roles.add(rolService.getByRolNombre(RolNombre.ROLE_USER).get());
        if(nuevoUsuario.getRoles().contains("admin"))
            roles.add(rolService.getByRolNombre(RolNombre.ROLE_ADMIN).get());
        usuario.setRoles(roles);
        usuarioService.save(usuario);
        return new ResponseEntity(new Mensaje("usuario guardado"), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<JwtDto> login(@Valid @RequestBody LoginUsuario loginUsuario, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity(new Mensaje("campos mal puestos"), HttpStatus.BAD_REQUEST);
        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUsuario.getNombreUsuario(), loginUsuario.getPassowrd()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateToken(authentication);
        UserDetails userDetails = (UserDetails)authentication.getPrincipal();
        JwtDto jwtDto = new JwtDto(jwt, userDetails.getUsername(), userDetails.getAuthorities());
        return new ResponseEntity(jwtDto, HttpStatus.OK);
    }
    
    @DeleteMapping("/usuarios/{id}")
    public void eliminarUsuario(@PathVariable(name="id")Integer id) {
    	usuarioService.deleteUsuario(id);
	}

	
}
