package com.magehotel.backend.apirest.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magehotel.backend.apirest.models.entity.HabitacionFecha;
import com.magehotel.backend.apirest.models.entity.Reserva;
import com.magehotel.backend.apirest.models.services.ReservaServiceImpl;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/api")
public class ReservaController {
	@Autowired
	ReservaServiceImpl reservaServiceImpl; 
	
	@GetMapping("/reservas")
	public List<Reserva> listarReserva(){
		return reservaServiceImpl.listReserva();
	}
	
	@GetMapping("/reservas/ocupados/{categoria}")
	public List<HabitacionFecha> diesReservats(@PathVariable(name="categoria") int categoria){
		List<Reserva> llistaReservas= reservaServiceImpl.listReserva();
		int lenght = llistaReservas.size();
		List<HabitacionFecha> llistaDates= new ArrayList<HabitacionFecha>();
		for (int i =0;i<lenght;i++) {
			if(llistaReservas.get(i).getHabitacion().getCategoria().getId()==categoria) {
				if(llistaReservas.get(i).getEstado()!=2) {
					HabitacionFecha afegir= new HabitacionFecha();
					LocalDate fechaInicio=llistaReservas.get(i).getFecha_inicio(); 
					LocalDate fechaFinal=llistaReservas.get(i).getFecha_final(); 
					/*System.out.println(fechaInicio+" "+fechaFinal);
					fechaInicio=fechaInicio.plusDays(1);
					fechaInicio=fechaInicio.plusDays(1);
					fechaInicio=fechaInicio.plusDays(1);
					fechaInicio=fechaInicio.plusDays(1);
					fechaInicio=fechaInicio.plusDays(1);
					if(fechaInicio.equals(fechaFinal)) {
						System.out.println("PUIPOOO");
					}
					System.out.println(fechaInicio+" "+fechaFinal);*/
					afegir.setHabitacion(llistaReservas.get(i).getHabitacion());
					afegir.setDiaReserva(fechaInicio);
					llistaDates.add(afegir);
					while(fechaInicio.equals(fechaFinal)==false) {
						HabitacionFecha afegir2= new HabitacionFecha();
						fechaInicio=fechaInicio.plusDays(1);
						//System.out.println(fechaInicio+" "+fechaFinal);
						afegir2.setHabitacion(llistaReservas.get(i).getHabitacion());
						afegir2.setDiaReserva(fechaInicio);
						System.out.println(afegir2.getDiaReserva());
						llistaDates.add(afegir2);
					}
				}
			}
		}
		return llistaDates;
	}

	
	
	@PostMapping("/reservas")
	public Reserva salvarReserva(@RequestBody Reserva reserva) {
		
		return reservaServiceImpl.saveReserva(reserva);
	}
	
	
	@GetMapping("/reservas/{id}")
	public Reserva reservaXID(@PathVariable(name="id") Integer id) {
		
		Reserva reserva_xid= new Reserva();
		
		reserva_xid=reservaServiceImpl.reservaXID(id);
		
		System.out.println("Reserva XID: "+reserva_xid);
		
		return reserva_xid;
	}
	
	@PutMapping("/reservas/{id}")
	public Reserva actualizarReserva(@PathVariable(name="id")Integer id,@RequestBody Reserva reserva) {
		
		Reserva reserva_seleccionado= new Reserva();
		Reserva reserva_actualizado= new Reserva();
		
		reserva_seleccionado= reservaServiceImpl.reservaXID(id);
		
		reserva_seleccionado.setCliente(reserva.getCliente());
		
		reserva_seleccionado.setHabitacion(reserva.getHabitacion());
		reserva_seleccionado.setFecha_inicio(reserva.getFecha_inicio());
		reserva_seleccionado.setFecha_final(reserva.getFecha_final());
		reserva_seleccionado.setEstado(reserva.getEstado());
		
		reserva_actualizado = reservaServiceImpl.updateReserva(reserva_seleccionado);
		
		System.out.println("La reserva actualizada es: "+ reserva_actualizado);
		
		return reserva_actualizado;
	}
	
	@PutMapping("/reservas/estado/{id}")
	public Reserva actualizarReservaEstado(@PathVariable(name="id")Integer id,@RequestBody Reserva reserva) {
		
		Reserva reserva_seleccionado= new Reserva();
		Reserva reserva_actualizado= new Reserva();
		
		reserva_seleccionado= reservaServiceImpl.reservaXID(id);
		reserva_seleccionado.setEstado(reserva.getEstado());
		
		reserva_actualizado = reservaServiceImpl.updateReserva(reserva_seleccionado);
		
		System.out.println("La reserva actualizada es: "+ reserva_actualizado);
		
		return reserva_actualizado;
	}
	
	@DeleteMapping("/reservas/{id}")
	public void eliminarHabitacion(@PathVariable(name="id")Integer id) {
		reservaServiceImpl.deleteReserva(id);
	}
}
