package com.magehotel.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magehotel.backend.apirest.models.entity.Habitacion;

public interface IHabitacionDao extends JpaRepository<Habitacion, Integer>{

}
