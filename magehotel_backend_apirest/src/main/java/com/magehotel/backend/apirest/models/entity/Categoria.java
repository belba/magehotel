package com.magehotel.backend.apirest.models.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="categorias")
public class Categoria {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "nombre", length=100, nullable = false)
	private String nombre;
	
	@Column(name = "aforo", nullable = false)
	private int aforo;
	
	@Column(name = "precio", nullable = false)
	private double precio;
	
	@OneToMany
    @JoinColumn(name="categoria")
	private List<Habitacion> habitacion;
	
	public Categoria() {
		
	}

	public Categoria(int id, String nombre, int aforo, List<Habitacion> habitacion, double precio ) {
		this.id = id;
		this.nombre = nombre;
		this.aforo = aforo;
		this.precio =  precio;
		this.habitacion = habitacion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getAforo() {
		return aforo;
	}

	public void setAforo(int aforo) {
		this.aforo = aforo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public List<Habitacion> getHabitacion() {
		return habitacion;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Habitacion")
	public void setHabitacion(List<Habitacion> habitacion) {
		this.habitacion = habitacion;
	}
	
	
}
