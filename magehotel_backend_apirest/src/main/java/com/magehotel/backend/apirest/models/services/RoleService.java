package com.magehotel.backend.apirest.models.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magehotel.backend.apirest.auth.enums.RolNombre;
import com.magehotel.backend.apirest.auth.repository.RoleRepository;
import com.magehotel.backend.apirest.models.entity.Rol;
import com.magehotel.backend.apirest.models.entity.Usuario;

@Service
@Transactional
public class RoleService {

    @Autowired
    RoleRepository rolRepository;

    public Optional<Rol> getByRolNombre(RolNombre rolNombre){
        return rolRepository.findByRolNombre(rolNombre);
    }

    public void save(Rol rol){
        rolRepository.save(rol);
    }
}
