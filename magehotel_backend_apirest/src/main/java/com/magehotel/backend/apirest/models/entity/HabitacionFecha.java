package com.magehotel.backend.apirest.models.entity;

import java.time.LocalDate;

public class HabitacionFecha {
	private Habitacion habitacion;
	private LocalDate diaReserva;
	public HabitacionFecha() {
		
	}
	public HabitacionFecha(Habitacion habitacion, LocalDate diaReserva) {
		super();
		this.habitacion = habitacion;
		this.diaReserva = diaReserva;
	}
	public Habitacion getHabitacion() {
		return habitacion;
	}
	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}
	public LocalDate getDiaReserva() {
		return diaReserva;
	}
	public void setDiaReserva(LocalDate diaReserva) {
		this.diaReserva = diaReserva;
	}
	
	

}
