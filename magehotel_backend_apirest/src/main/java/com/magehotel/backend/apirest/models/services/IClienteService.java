package com.magehotel.backend.apirest.models.services;

import java.util.List;

import com.magehotel.backend.apirest.models.entity.Cliente;

public interface IClienteService {

	public List<Cliente> getAllClientes();

	public Cliente getCliente(Long id);

	public Cliente saveCliente(Cliente cliente);

	public Cliente updateCliente(Cliente cliente);

	public void deleteCliente(Long id);

}
