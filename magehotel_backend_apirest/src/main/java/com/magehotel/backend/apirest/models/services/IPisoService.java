package com.magehotel.backend.apirest.models.services;

import java.util.List;

import com.magehotel.backend.apirest.models.entity.Piso;

public interface IPisoService {
	
	public List<Piso> listPiso(); 
	
	public Piso savePiso(Piso piso);	
	
	public Piso pisoXID(Integer id); 
	
	public Piso updatePiso(Piso piso); 
	
	public void deletePiso(Integer id);

}
