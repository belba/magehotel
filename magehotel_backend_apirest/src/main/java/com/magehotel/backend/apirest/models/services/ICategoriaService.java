package com.magehotel.backend.apirest.models.services;

import java.util.List;

import com.magehotel.backend.apirest.models.entity.Categoria;



public interface ICategoriaService {
	public List<Categoria> listCategoria(); 
	
	public Categoria saveCategoria(Categoria categoria);	
	
	public Categoria categoriaXID(Integer id); 
	
	public Categoria updateCategoria(Categoria categoria); 
	
	public void deleteCategoria(Integer id);
}
