package com.magehotel.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magehotel.backend.apirest.models.dao.IPisoDao;
import com.magehotel.backend.apirest.models.entity.Piso;

@Service
public class PisoServiceImpl implements IPisoService {

	@Autowired
	IPisoDao iPisoDAO;
	@Override
	public List<Piso> listPiso() {
		// TODO Auto-generated method stub
		return iPisoDAO.findAll();
	}

	@Override
	public Piso savePiso(Piso piso) {
		// TODO Auto-generated method stub
		return iPisoDAO.save(piso);
	}

	@Override
	public Piso pisoXID(Integer id) {
		// TODO Auto-generated method stub
		return iPisoDAO.findById(id).get();
	}

	@Override
	public Piso updatePiso(Piso piso) {
		// TODO Auto-generated method stub
		return iPisoDAO.save(piso);
	}

	@Override
	public void deletePiso(Integer id) {
		// TODO Auto-generated method stub
		iPisoDAO.deleteById(id);
	}

}
