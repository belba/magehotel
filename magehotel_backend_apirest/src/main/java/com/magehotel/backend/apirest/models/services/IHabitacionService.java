package com.magehotel.backend.apirest.models.services;

import java.util.List;

import com.magehotel.backend.apirest.models.entity.Habitacion;

public interface IHabitacionService {
	public List<Habitacion> listHabitacion(); 
	
	public Habitacion saveHabitacion(Habitacion habitacion);	
	
	public Habitacion habitacionXID(Integer id); 
	
	public Habitacion updateHabitacion(Habitacion habitacion); 
	
	public void deleteHabitacion(Integer id);
	
}
