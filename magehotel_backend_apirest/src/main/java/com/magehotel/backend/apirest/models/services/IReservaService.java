package com.magehotel.backend.apirest.models.services;

import java.util.List;

import com.magehotel.backend.apirest.models.entity.Reserva;

public interface IReservaService {

public List<Reserva> listReserva(); 
	
	public Reserva saveReserva(Reserva reserva);	
	
	public Reserva reservaXID(Integer id); 
	
	public Reserva updateReserva(Reserva reserva); 
	
	public void deleteReserva(Integer id);
}
