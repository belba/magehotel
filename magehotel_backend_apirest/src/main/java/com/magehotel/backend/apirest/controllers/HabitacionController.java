package com.magehotel.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magehotel.backend.apirest.models.entity.Habitacion;

import com.magehotel.backend.apirest.models.services.HabitacionServiceImpl;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/api")
public class HabitacionController {
	@Autowired
	HabitacionServiceImpl habitacionServiceImpl; 
	
	@GetMapping("/habitaciones")
	public List<Habitacion> listarHabitacion(){
		return habitacionServiceImpl.listHabitacion();
	}
	
	@GetMapping("/habitaciones/num/{categoria}")
	public Integer numHabitaciones(@PathVariable(name="categoria") Integer categoria){
		List<Habitacion> llista=habitacionServiceImpl.listHabitacion();
		int numHabitaciones=0;
		for(int i=0;i<llista.size();i++) {
			if(llista.get(i).getCategoria().getId()==categoria) {
				numHabitaciones++;
			}
		}
		return numHabitaciones;
	}
	
	@PostMapping("/habitaciones")
	public Habitacion salvarHabitacion(@RequestBody Habitacion habitacion) {
		
		return habitacionServiceImpl.saveHabitacion(habitacion);
	}
	
	
	@GetMapping("/habitaciones/{id}")
	public Habitacion habitacionXID(@PathVariable(name="id") Integer id) {
		
		Habitacion habitacion_xid= new Habitacion();
		
		habitacion_xid=habitacionServiceImpl.habitacionXID(id);
		
		System.out.println("Habitacion XID: "+habitacion_xid);
		
		return habitacion_xid;
	}
	
	
	@PutMapping("/habitaciones/{id}")
	public Habitacion actualizarHabitacion(@PathVariable(name="id")Integer id,@RequestBody Habitacion habitacion) {
		
		Habitacion habitacion_seleccionado= new Habitacion();
		Habitacion habitacion_actualizado= new Habitacion();
		
		habitacion_seleccionado= habitacionServiceImpl.habitacionXID(id);
		
		habitacion_seleccionado.setPiso(habitacion.getPiso());
		
		habitacion_seleccionado.setPuerta(habitacion.getPuerta());
		habitacion_seleccionado.setCategoria(habitacion.getCategoria());
		habitacion_seleccionado.setDetalles(habitacion.getDetalles());
		habitacion_seleccionado.setEstado(habitacion.getEstado());
		
		habitacion_actualizado = habitacionServiceImpl.updateHabitacion(habitacion_seleccionado);
		
		System.out.println("La habitacion actualizada es: "+ habitacion_actualizado);
		
		return habitacion_actualizado;
	}
	
	@DeleteMapping("/habitaciones/{id}")
	public void eliminarHabitacion(@PathVariable(name="id")Integer id) {
		habitacionServiceImpl.deleteHabitacion(id);
	}

}
