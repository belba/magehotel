package com.magehotel.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magehotel.backend.apirest.models.dao.IClienteDao;
import com.magehotel.backend.apirest.models.entity.Cliente;

@Service
public class ClienteServiceImpl implements IClienteService{

	@Autowired
	IClienteDao clienteDao;
	
	@Override
	public List<Cliente> getAllClientes() {
		return clienteDao.findAll();
	}

	@Override
	public Cliente getCliente(Long id) {
		return clienteDao.findById(id).get();
	}

	@Override
	public Cliente saveCliente(Cliente cliente) {
		return clienteDao.save(cliente);
	}

	@Override
	public Cliente updateCliente(Cliente cliente) {
		return clienteDao.save(cliente);
	}

	@Override
	public void deleteCliente(Long id) {
		clienteDao.deleteById(id);
	}

}
