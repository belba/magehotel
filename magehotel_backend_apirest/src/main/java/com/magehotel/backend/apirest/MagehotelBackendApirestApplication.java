package com.magehotel.backend.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MagehotelBackendApirestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MagehotelBackendApirestApplication.class, args);
	}

}
