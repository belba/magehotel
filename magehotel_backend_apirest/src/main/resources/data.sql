INSERT INTO clientes (id,dni,nombre) VALUES (1, '699999S', 'Juan');
INSERT INTO clientes (id,dni,nombre) VALUES (2, '888885T', 'Samuel');

INSERT INTO categorias (id,nombre,aforo) VALUE (1,"Económico",50);
INSERT INTO categorias (id,nombre,aforo) VALUE (2,"Superior",30);
INSERT INTO categorias (id,nombre,aforo) VALUE (3,"Excepcional",10);

INSERT INTO pisos (id,nombre) VALUE (1,"PISO 1");
INSERT INTO pisos (id,nombre) VALUE (2,"PISO 2");
INSERT INTO pisos (id,nombre) VALUE (3,"PISO 3");
INSERT INTO pisos (id,nombre) VALUE (4,"PISO 4");
INSERT INTO pisos (id,nombre) VALUE (5,"PISO 5");